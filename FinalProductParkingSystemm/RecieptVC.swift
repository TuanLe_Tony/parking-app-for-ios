//
//  RecieptVC.swift
//  FinalProductParkingSystemm
//
//  Created by levantuan on 2018-03-06.
//  Copyright © 2018 levantuan. All rights reserved.
//

import UIKit

class RecieptVC: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.ColorPicker.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return self.ColorPicker[row]
    }
    
    
    
    @IBOutlet weak var txtEmailAddress: UITextField!
    @IBOutlet weak var txtCarPNumber: UITextField!
    @IBOutlet weak var txtCarCompany: UITextField!
    @IBOutlet weak var txtCarColor: UITextField!
    @IBOutlet weak var txtNoofHP: UITextField!
    @IBOutlet weak var txtLotNumber: UITextField!
    @IBOutlet weak var txtSpotNumber: UITextField!
    @IBOutlet weak var txtPaymentM: UITextField!
    @IBOutlet weak var txtPaymentA: UITextField!
    
    @IBOutlet weak var CarColorPicker: UIPickerView!
    
    
    @IBOutlet weak var DATE: UITextField!
    
    var ColorPicker: [String] = ["Red", "White", "Blue", "Green", "Grey", "Brown", "Pink", "Black", "Orange","Yellow", "purple"]
    var selectedColorindex:Int = 10
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.CarColorPicker.delegate  = self
        self.CarColorPicker.dataSource = self

        // Do any additional setup after loading the view.
    }
   /* override func viewWillAppear(_ animated: Bool) {
    self.navigationController!.setNavigationBarHidden(false, animated: true)
        self.title = "Receipt Payment"
        
        let barbutton = UIBarButtonItem(title: "Sign", style: .plain, target: self, action: #selector(displayPicker))
        self.navigationItem.rightBarButtonItem = barbutton
    }
*/
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func Save(_ sender: UIBarButtonItem) {
        if((txtEmailAddress.text?.isEmpty)! || (txtCarPNumber.text?.isEmpty)! || (txtCarPNumber.text?.isEmpty)! || (txtCarColor.text?.isEmpty)! || (txtNoofHP.text?.isEmpty)! || (txtLotNumber.text?.isEmpty)! || (txtPaymentM.text?.isEmpty)! || (txtPaymentA.text?.isEmpty)! || (DATE.text?.isEmpty)!){
            displayMyAlertMessage(UserMessage: "Please Field ")
        }
        
        print("Selecte value : \(ColorPicker[selectedColorindex])")
        
        if txtCarPNumber.text != UserDefaults.standard.object(forKey: "C") as? String
        {
            displayMyAlertMessage(UserMessage: "Your Car Plate Number does not match with your Profiles")
            return 
        }
        if txtEmailAddress.text != UserDefaults.standard.object(forKey: "D") as? String
        {
            displayMyAlertMessage(UserMessage: "Your Email does not match with your Profiles")
            return
        }
        
    performSegue(withIdentifier: "Pass", sender: self)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? ReceiptListVC {
            destination.Email = txtEmailAddress.text!
            destination.CarPNumber = txtCarPNumber.text!
            destination.CarComPaNy = txtCarCompany.text!
            destination.CarCoLor = txtCarColor.text!
            destination.NoOFHP = txtNoofHP.text!
            destination.PayMentA = txtPaymentA.text!
            destination.PayMentM = txtPaymentM.text!
            destination.SpotNUMBER  = txtSpotNumber.text!
            destination.LotNUMBER = txtLotNumber.text!
            destination.DATE = DATE.text!
            destination.picker1 = ColorPicker[selectedColorindex]
        
            
            //let RCListSB:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            //let RClistSV = RCListSB.instantiateViewController(withIdentifier: "ReceiptListScreen")
            //navigationController?.pushViewController(RClistSV, animated: true)
        }
        
    }
    func displayMyAlertMessage(UserMessage: String)
    {
        let MyAlert = UIAlertController(title: "Register", message: UserMessage, preferredStyle: UIAlertControllerStyle.alert);
        
        let Action = UIAlertAction(title: "Okey", style: UIAlertActionStyle.default, handler:nil)
        
        MyAlert.addAction(Action);
        self.present(MyAlert, animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
        @objc private func displayPicker() {
            self.selectedColorindex = self.CarColorPicker.selectedRow(inComponent: 10)
            print("Selecte index : \(selectedColorindex)")
            
          //  let alldata: String = "\(txtEmailAddress.text!) \n \(txtCarPNumber.text!) \n \(txtCarColor.text!) \n \(txtNoofHP.text!) \n \(txtPaymentA.text!) \n \(txtPaymentM.text!) \n \(txtLotNumber.text!) \n \(txtSpotNumber.text!)"
            
        }
}


